import java.io._
import java.security.InvalidKeyException
import javax.crypto.spec.SecretKeySpec
import javax.crypto.{BadPaddingException, Cipher, CipherInputStream}

import org.apache.commons.cli.{DefaultParser, HelpFormatter, Option, Options}

import scala.util.Try

object Main {
  private val EncryptionAlgorithm = "Blowfish"
  private val Transformation = "Blowfish/ECB/PKCS5Padding"

  def cliOptions(): Options = {
    val helpOption = Option.builder("h")
      .longOpt("help")
      .desc("Prints this help message")
      .build()

    val fileOption = Option.builder("f")
      .longOpt("file")
      .desc("Specifies the file to encrypt/decrypt")
      .hasArg
      .build()

    val keepOption = Option.builder("k")
      .longOpt("keep")
      .desc("Do not delete the input file after encryption")
      .build()

    new Options()
      .addOption(helpOption)
      .addOption(fileOption)
      .addOption(keepOption)
  }

  private def openInputStream(filePath: String, cipher: Cipher): InputStream = {
    val inputStream = Try(new FileInputStream(filePath)).fold(
      throwable => {
        throwable match {
          case _: FileNotFoundException => fatalError(s"File '$filePath' not found")
          case _: SecurityException => fatalError(s"File '$filePath': permission denied")
          case _ => fatalError("Unexpected error")
        }
        None
      },
      inputStream => Some(inputStream)
    ).get

    new CipherInputStream(inputStream, cipher)
  }

  private def openOutputStream(filePath: String): OutputStream =
    Try(new FileOutputStream(filePath)).fold(
      throwable => {
        throwable match {
          case _: FileNotFoundException => fatalError(s"File '$filePath' cannot be opened")
          case _: SecurityException => fatalError(s"File '$filePath': permission denied")
          case _ => fatalError("Unexpected error")
        }
        None
      },
      inputStream => Some(inputStream)
    ).get

  def main(args: Array[String]): Unit = {
    setupSecurity()

    val options = cliOptions()
    val parser = new DefaultParser()

    val cmd = parser.parse(options, args)
    if (cmd.hasOption("help")) {
      val formatter = new HelpFormatter
      formatter.printHelp("java -jar bcrypt.jar", options, true)
    } else if (cmd.hasOption("file")) {
      val filePath = cmd.getOptionValue("file")

      val file = new File(filePath)
      if (!file.exists) {
        fatalError(s"File '$filePath' not found")
      }
      if (!file.isFile) {
        fatalError(s"'$filePath' is not a file")
      }

      val key = System.console().readPassword("Enter encryption key: ").map(_.toByte)
      processFile(filePath, key)

      if (!cmd.hasOption("keep")) {
        new File(filePath).delete()
      }
    } else {
      fatalError("Missing 'file' argument")
    }
  }

  private def getCipher(key: Array[Byte], mode: Int): Cipher = {
    val keySpec = new SecretKeySpec(key, EncryptionAlgorithm);

    val cipher = Cipher.getInstance(Transformation)
    try {
      cipher.init(mode, keySpec)
    } catch {
      case x: InvalidKeyException if x.getMessage.contains("Key too long") => fatalError("The key is too long for blowfish (max 56 characters)")
    }

    cipher
  }

  private def fatalError(message: String): Unit = {
    println(s"[error] $message")
    System.exit(-1)
  }

  private def processFile(filePath: String, key: Array[Byte]): Unit = {
    val (outputFilePath, cipherMode) = if (filePath.endsWith(".bcrypt")) {
      (filePath.stripSuffix(".bcrypt"), Cipher.DECRYPT_MODE)
    } else {
      val verificationKey = System.console().readPassword("Enter encryption key again: ").map(_.toByte)
      if (!(key sameElements verificationKey)) {
        fatalError("Entered keys do not match")
      }
      (s"$filePath.bcrypt", Cipher.ENCRYPT_MODE)
    }

    val inputStream = openInputStream(filePath, getCipher(key, cipherMode))
    val outputStream = openOutputStream(outputFilePath)

    try {
      var byteRead = inputStream.read()
      while (byteRead != -1) {
        outputStream.write(byteRead)
        byteRead = inputStream.read()
      }
    } catch {
      case _: BadPaddingException => fatalError("Corrupted file or incorrect key")
      case _: IOException => fatalError("Corrupted file or incorrect key")
    }

    inputStream.close()
    outputStream.flush()
    outputStream.close()
  }

  private def setupSecurity(): Unit = {
    try {
      val field = Class.forName("javax.crypto.JceSecurity")
        .getDeclaredField("isRestricted")

      val modifiers = classOf[java.lang.reflect.Field].getDeclaredField("modifiers")
      modifiers.setAccessible(true)
      modifiers.setInt(field, field.getModifiers & ~java.lang.reflect.Modifier.FINAL)

      field.setAccessible(true)
      field.setBoolean(null, false)
      field.setAccessible(false)
    } catch {
      case e: Throwable => e.printStackTrace()
    }
  }
}
