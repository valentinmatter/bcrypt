import com.github.retronym.SbtOneJar._

name := "bcrypt"

version := "1.0"

scalaVersion := "2.12.3"

oneJarSettings

libraryDependencies += "commons-cli" % "commons-cli" % "1.3.1"
